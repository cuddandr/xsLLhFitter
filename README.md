# Super-xsllhFitter

## Introduction

The goal of the Super-xsllhFitter is to provide a general purpose binned maximum likelihood-based fit framework for performing neutrino cross section analyses (currently focused on T2K analyses). The current state of the code and documentation is very much work in progress and is ~~really not~~ kind of ready for general use. However, if you are desperate or can't wait then feel free to check out the code! There is no guarantee the fitter will be able to perform your analysis.

The code is still under (semi-)active development and there is no guarantee that future commits will retain backward compatibility. Be careful when merging changes into an in-progress analysis.

Documentation related to the Super-xsllhFitter is available here as a webpage: [https://cuddandr.gitlab.io/xsLLhFitter/](https://cuddandr.gitlab.io/xsLLhFitter/) and available in the `docs/` directory as a series of Markdown files.

Note that the documentation is also a work in progress.
